#ifndef _QUE_OOP_H
#define _QUE_OOP_H

#define FALSE 0
#define TRUE 1

typedef unsigned char cbool;
typedef unsigned char uint8_t;

#define QUE_MAX_LEN 100

typedef char QUEUE_TYPE;
struct Que_vtable;

/*创建循环队列类*/

/*循环队列实际上只有K-1个空间能用*/
/*队列为空：头=尾 */
/*队列满：头=尾+1%最大长度，留出一个空位作为满的条件，不然头=尾的情况和空的情况重复*/

/*循环队列结构体*/
typedef struct Cir_queue
{
    struct Que_vtable *c_vptr;

    QUEUE_TYPE  Queue_Buffer[QUE_MAX_LEN];//缓冲数组
    int head,tail,max_len,lenth;
}Cir_queue;

/*队列虚表*/
struct Que_vtable
{
    // void (*create_queue)(void * Me);
    void (*delete_queue)(void * const Me);

    cbool (*empty)(void const * const Me);
    cbool (*full)(void const * const Me);

    cbool (*pop)(void * const Me,QUEUE_TYPE *Get); 
    cbool (*push)(void * const Me,QUEUE_TYPE value);

    cbool (*head_push)(void * const Me,QUEUE_TYPE value);
    cbool (*tail_pop)(void * const Me,QUEUE_TYPE *Get); 

    cbool (*back)(void const * const Me,QUEUE_TYPE *Get);
    cbool (*front)(void const * const Me,QUEUE_TYPE *Get);  

    int   (*lenth)(void const * const Me);

}Que_vtable;

/*多态类*/
// void Qcreate_queue(void * const Me);
void Qdreate_queue(void * const Me);
cbool Qempty(void const * const Me);
cbool Qfull(void const * const Me);
cbool Qtail_pop(void * const Me,QUEUE_TYPE *Get);
cbool Qhead_push(void * const Me,QUEUE_TYPE value);
cbool Qpop(void * const Me,QUEUE_TYPE *Get);
cbool Qpush(void * const Me,QUEUE_TYPE value);
cbool Qback(void const * const Me,QUEUE_TYPE *Get);
cbool Qfront(void const * const Me,QUEUE_TYPE *Get);
int Q_lenth(void const * const Me);

/*循环队列*/
void Cir__Qcreate_queue(Cir_queue * const Me);
void Cir__Qdreate_queue(Cir_queue * const Me);
cbool Cir__Qempty(Cir_queue const * const Me);
cbool Cir__Qfull(Cir_queue const * const Me);
cbool Cir_Qtail_pop(Cir_queue * const Me,QUEUE_TYPE *Get);
cbool Cir_Qhead_push(Cir_queue * const Me,QUEUE_TYPE value);
cbool Cir__Qpush(Cir_queue * const Me,QUEUE_TYPE value);
cbool Cir__Qpop(Cir_queue * const Me,QUEUE_TYPE *Get);
cbool Cir__Qback(Cir_queue const * const Me,QUEUE_TYPE *Get);
cbool Cir__Qfront(Cir_queue const * const Me,QUEUE_TYPE *Get);
int Cir__Q_lenth(Cir_queue const * const Me);

void * my_memset(void *source,int dest,int n);
#endif
