#ifndef _USART_STM_H
#define _USART_STM_H

#include "cir_queue.h"

/*协议有限状态机枚举*/
typedef enum
{
    STATUS_IDEL=(uint8_t) 0,
    STATUS_HEAD,
    STATUS_DATA,
    STATUS_END,
}COM_STATUS;

#define COMMAND_LIST_MAX 10 /*命令列表长度*/
#define COMMAND_LEN_MAX 20  /*命令最大长度*/

struct Usart_vtable;
/*协议有限状态机结构体*/
typedef struct Usart_Trm
{
    /*有效帧格式： (data),不支持中文输入*/
	uint8_t  Uartx_frame;   /*有效数据帧数*/
	uint8_t head;           /*头帧标志*/
	uint8_t teal;            /*尾帧标志*/
    COM_STATUS usart_status;  /*状态机*/
    int last_pos;           /*错误数据计数*/
    
    struct Usart_vtable *c_vptr;/*函数表，提供接口接口使用*/
}Usart_Trm;

struct Usart_vtable
{
    // void (*create_ComUsart)(Usart_Trm *Me,uint8_t set_h,uint8_t set_t);
    void (*rx_buff)(Cir_queue * const QMe,Usart_Trm *const Me,uint8_t bydata);

    int (*fram_num)(Usart_Trm *Me);
    long (*fram_deal)(Cir_queue *QMe,Usart_Trm *Me,const char *sdata);

}Usart_vtable;

void Create_ComUsart(Usart_Trm *Me,uint8_t set_h,uint8_t set_t);
void Com_rxUsart_data(Cir_queue * const QMe,Usart_Trm *const Me,uint8_t bydata);

int Get_fram_num(Usart_Trm *Me);
long Deal_com(Cir_queue *QMe,Usart_Trm *Me,const char *sdata);

void Com_tailpush(Cir_queue *QMe,Usart_Trm *Me,char const *get_str);
#endif
