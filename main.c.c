#include <windows.h>
#include <stdio.h>
#include "ustate_machine.h"

int main()
{
    long x;
    Usart_Trm usart_trm;
    Cir_queue que;
    int cout=0;

    /*假设实际接收到的数据*/
    char AS[20]="(PID_P=1.11)";
    char BS[20]="(PID_I=2.22)";
    char CS[20]="(PID_D=3.33)";

    char COM[20]="(SET)";

    Create_ComUsart(&usart_trm,'(',')');
    Cir__Qcreate_queue(&que);


    /*模拟串口存入数据*/
    for(int j=0;j<5;j++)
    {
        for(int i=0;i<strlen(AS);i++)
        {
            /*逐个数据存入*/
            if(j==0) usart_trm.c_vptr->rx_buff(&que,&usart_trm,AS[i]);/*PID_P*/
            if(j==1) usart_trm.c_vptr->rx_buff(&que,&usart_trm,BS[i]);/*PID_I*/
            if(j==2) usart_trm.c_vptr->rx_buff(&que,&usart_trm,CS[i]);/*PID_D*/
            if(j==3) usart_trm.c_vptr->rx_buff(&que,&usart_trm,AS[i]);/*PID_P*/
            if(j==4) usart_trm.c_vptr->rx_buff(&que,&usart_trm,COM[i]);/*SET*/
        }
    }

    if(Get_fram_num(&usart_trm))
    {
        /*调参模式*/
        /*传入指令*/
        x=usart_trm.c_vptr->fram_deal(&que,&usart_trm,"PID_D=");
        printf("x=%d\n",x);                                     
        x=usart_trm.c_vptr->fram_deal(&que,&usart_trm,"PID_D=");
        printf("x=%d\n",x);                                     
        x=0;
        x=usart_trm.c_vptr->fram_deal(&que,&usart_trm,"PID_I=");
        printf("x=%d\n",x);                                     
        x=0;    
        x=usart_trm.c_vptr->fram_deal(&que,&usart_trm,"PID_P=");
        printf("x=%d\n",x);                                     
        x=0;
        /*命令模式*/
        x=usart_trm.c_vptr->fram_deal(&que,&usart_trm,"SET");
        printf("x=%d\n",x);                                     
                     
    }
    system("pause");
}
